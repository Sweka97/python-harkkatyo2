# CT60A0201 Ohjelmoinnin perusteet 2017 ohjelmien otsikkotiedot.
# Tekijä: Jesse Peltola
# Opiskelijanumero: 0523140
# Päivämäärä: 10.12.2017
# Yhteistyö ja lähteet, nimi ja yhteistyön muoto: mathplotlib.org, Python 3 Ohjelmointiopas versio 1.1
# HUOM! KAIKKI KURSSIN TEHTÄVÄT OVAT HENKILÖKOHTAISIA!
######################################################################
#Tuodaan matplotlib ja numpy
#import matplotlib.pyplot as plt
#import numpy as np

#Luodaan luokka
class taksi():
    kelloaika = ""
    
#Luetaan ensimmäinen datatiedosto
def lue_eka(lista):
    rivi_maara = 0
    tiedosto = input("Anna ensimmäinen matkadataa sisältävän tiedoston nimi: ")
    try: #Poikkeustenkäsittely
        with open(tiedosto, "r", encoding ="utf-8") as tdsto:
            kuukausi = input("Anna datalle kuvaava nimi: ")
            
            for i in tdsto:
                tksi = taksi()
                rivi = i.split(",")
                if (i[0].isalpha()): #Jos ensimmäinen rivi sisältää kirjaimia jatketaan seuraavalle riville
                    continue
                tksi.kelloaika = rivi[1][11:13] 
                lista.append(tksi)
                rivi_maara += 1
                
            print("Tiedosto luettu, rivejä", rivi_maara)
            
    except IOError:
        print("Virheellinen tiedosto.")

    return lista, kuukausi #Palauttaa listan, jossa on kelloaikoja ja kuukauden
    
    
def lisaa(lista, lista1):
    tiedosto = input("Anna seuraava matkadataa sisältävän tiedoston nimi: ")
    
    try:
        with open(tiedosto, "r", encoding = "utf-8") as tdsto:
            rivi_maara = 0
            kuukausi1 = input("Anna datalle kuvaava nimi: ")  
            for i in tdsto:
                tksi = taksi()
                rivi = i.split(",")
                if (i[0].isalpha()):
                    continue
                tksi.kelloaika = rivi[1][11:13]
                lista1.append(tksi)
                rivi_maara += 1
              
            print("Tiedosto luettu, rivejä", rivi_maara)        
            

    except IOError:
        print("Virheellinen tiedosto.")

    return lista, lista1, kuukausi1 #Paluattaa ensimmäisen tiedoston listan ja kuukauden, toisen tiedoston listan ja kuukauden 
    
def kirjoita(lista, y):
    tiedosto = input("Anna kirjoitettavan frekvenssi CSV-tiedoston nimi: ")
    try:
        with open(tiedosto, "w") as tdsto1:
            kello = []
            matkat = {0:0, 1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0, 10:0, 11:0, 12:0, 13:0, 14:0, 15:0, 16:0, 17:0, 18:0, 19:0, 20:0, 21:0, 22:0, 23:0}
            matkat1 = {0:0, 1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0, 10:0, 11:0, 12:0, 13:0, 14:0, 15:0, 16:0, 17:0, 18:0, 19:0, 20:0, 21:0, 22:0, 23:0}

            #Lisätään kelloaikoja listaan ja samalla vertaillaan kelloaikoja.                                          
            for i in range(0, 24):
                kello.append(str(i))
                if y == 0: #ensimmäinen tiedosto
                    for x in lista[0]:
                        if (int(x.kelloaika) == i):
                            matkat[i] += 1
                elif y == 1: #ensimmäisen ja toisen tiedosto
                    for x in lista[0][0]:
                        if (int(x.kelloaika) == i):
                            matkat[i] += 1    
                    for a in lista[1]:
                        if (int(a.kelloaika) == i):
                            matkat1[i] += 1
                                                         
            if y == 1: #Sama juttu
                #Kirjoitetaan kuukausi CSV #1
                teksti1 = "" + ";" + str(lista[0][1]) + ";" + str(lista[2]) + "\n"
                tdsto1.write(teksti1)
                #Kirjoitetaan kelloajat ja matkojen määrä CSV #2
                for i in range(len(matkat1)):
                    teksti3 = str(i) + ";" + str(matkat[i]) + ";" + str(matkat1[i]) + "\n"
                    tdsto1.write(teksti3)
            if y == 0:
                #Sama kuin #1
                teksti2 = "" + ";" + str(lista[1]) + "\n"
                tdsto1.write(teksti2)
                #Sama kuin #2
                for n in range(len(matkat)):
                    teksti4 = str(n) + ";" + str(matkat[n]) + "\n"
                    tdsto1.write(teksti4)
    except IOError:
        print("Virheellinen tiedosto!")
    if (y == 0):
        return matkat, lista[1]
    if (y == 1):
        return matkat, matkat1, lista[0][1], lista[2]

def piirra(lista):
    #Luodaan listat, johon tulee matkojen määrää
    matkojen_maara1 = []
    matkojen_maara2 = []
    
    matkat = lista[0]
    matkat1 = lista[1]
    kuukausi1 = lista[2]
    kuukausi2 = lista[3]
    
    maara = len(matkat)
    ind = np.arange(maara)   
    width = 0.30
    
    fig, tiedot = plt.subplots()

    for i in range(len(matkat)):
        matkojen_maara1.append(matkat[i])
        matkojen_maara2.append(matkat1[i])
            
    matkat1 = tiedot.bar(ind, matkojen_maara1, width, color = 'r', label = str(kuukausi1))
    matkat2 = tiedot.bar(ind+width, matkojen_maara2, width, color = 'y', label = str(kuukausi2))
        
    tiedot.set_ylabel('Matkojen määrä')
    tiedot.set_xlabel('Kellonaika')
    tiedot.set_title('Matkojen määrä eri kelloaikoina')
    tiedot.set_xticks(ind + width / 2)
    tiedot.set_xticklabels((matkat))
    tiedot.legend(loc = 1)

    plt.show()
    
def valikko():
    #Luodaan valikko
    print("Anna haluamasi toiminnon numero seuraavasta valikosta: \n"
          "1) Lue ensimmäinen matkadatatiedosto \n"
          "2) Lisää matkadatatiedosto \n"
          "3) Kirjoita csv \n"
          "4) Piirrä kuvaaja \n"
          "0) Lopeta")
    
    valinta = int(input("Valintasi: "))
    return valinta

def main():
    lista = []
    lista1 = []
    x = 0 #Muuttuja joka katsoo käykö ensimmäinen tiedoston ja kirjoitetaan tiedostoon vai ensimmäisen tiedoston, toisen tiedoston ja sitten kirjoittaa csv
    
    while True:
        val = valikko()
        if (val == 1):
            lue = lue_eka(lista)
        elif (val == 2):
            lisays = lisaa(lue, lista1)
            x = 1 #Tämän avulla tiedetään onko toista tiedostoa luettu
        elif (val == 3):
            if (x == 0): #Toista tiedostoa ei ole luettu niin käsitellään vaan ensimmäinen tiedosto
                kirjoitus = kirjoita(lue, x)
            elif(x == 1): #Jos toinen tiedosto on myös luettu niin käsitellää molemmat tiedostot
                kirjoitus = kirjoita(lisays, x)
        elif (val == 4):
            piirto = piirra(kirjoitus)
        elif (val == 0):
            print("Kiitos ohjelman käytöstä.")
            break
        else:
            print("Virheellinen syöttö!")
if __name__ == "__main__":
	main()
######################################################################
# eof

