# CT60A0201 Ohjelmoinnin perusteet 2017 ohjelmien otsikkotiedot.
# Tekijä: Jesse Peltola
# Opiskelijanumero: 0523140
# Päivämäärä: 10.12.2017
# Yhteistyö ja lähteet, nimi ja yhteistyön muoto:
# HUOM! KAIKKI KURSSIN TEHTÄVÄT OVAT HENKILÖKOHTAISIA!
######################################################################

#Tuodaan HT2lib.py tiedostosta luokka, aliohjelmat, valikko ja paaohjelma
from HT2lib import taksi, valikko, lue_eka, lisaa, piirra, main, kirjoita

#Jos pääohjelman nimi on main niin käynnistetään pääohjelma.
if __name__ == '__main__':
    main()

######################################################################
# eof

